NAMESPACE = internaltools
NAME = todoer
CONTAINER = $(NAMESPACE)/$(NAME)
BUILD_PARAMS =
PYTEST_ARGS = .
DOCKER_RUN_PARAMS = -d -P
POSTGRES_CONTAINER = todoer_postgres

build:
	@echo "Building container $(CONTAINER)..."
	docker build -t $(CONTAINER) $(BUILD_PARAMS) .

run: stop
	@echo "Starting container..."
	docker run --name $(NAME) $(DOCKER_RUN_PARAMS) $(CONTAINER)

stop:
	@echo "Stopping and removing container..."
	@docker stop $(NAME) || true
	@docker rm $(NAME) || true


setupdb:
	@echo Starting a postgres container...
	@docker run -e POSTGRES_DB=todoer -p 5432:5432 -d --name $(POSTGRES_CONTAINER) postgres
