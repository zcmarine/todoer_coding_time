FROM python:3.5

ENV PROJECT_DIR=/opt/code \
  PYTHON_PACKAGE_INDEX=https://devpi.squarespace.net/root/sqsp \
  PROJECT_NAME=todoer \
  PROJECT_USER=app \
  PROJECT_PORT=8080

EXPOSE $PROJECT_PORT

ARG PYTHON_REQUIREMENTS=requirements.txt
COPY requirements*.txt /tmp/
RUN pip install  --no-cache-dir -r /tmp/$PYTHON_REQUIREMENTS -i $PYTHON_PACKAGE_INDEX

COPY . $PROJECT_DIR
RUN groupadd -r $PROJECT_USER \
  && useradd -r -g $PROJECT_USER $PROJECT_USER \
  && chmod -R a+r $PROJECT_DIR

USER $PROJECT_USER
WORKDIR $PROJECT_DIR

CMD uwsgi conf.ini
